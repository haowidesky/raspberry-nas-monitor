
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gpio.h"
#include "log.h"


static bool is_file_exist ( const char *file_path )
{
	if ( NULL == file_path ){
		return FALSE;
	}
	if ( access ( file_path , F_OK ) ){
		return FALSE;
	}

	return TRUE;
} 


bool Gpio::open()
{
	char buffer[BUFFER_MAX];
	char path[DIRECTION_MAX];
	int len;
	int fd;

	if ( gpio_pin == GPIO_PIN_NONE ){
		return FALSE;
	}
	
	// 判断gpio是否已经export
	snprintf ( path , DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", gpio_pin );
	if ( TRUE == is_file_exist ( path ) )
		return TRUE;

	fd = ::open( "/sys/class/gpio/export" , O_WRONLY );
	if (fd < 0) {
		log_printf(LOG_ERROR, "Failed to open export for writing!\r\n");
		return FALSE;
	}

	len = snprintf(buffer, BUFFER_MAX, "%d", gpio_pin);
	if ( ::write ( fd, buffer, len ) < 0 ) {
		log_printf(LOG_ERROR, "Fail to export gpio%d!\r\n", gpio_pin);
		::close(fd);
		return FALSE;
	}

	::close(fd);
	return TRUE;
}


bool Gpio::close()
{
	char buffer[BUFFER_MAX];
	char path[DIRECTION_MAX];
	int len;
	int fd;

	if ( gpio_pin == GPIO_PIN_NONE ){
		return FALSE;
	}

	// 判断gpio是否已经export
	snprintf ( path , DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", gpio_pin );
	if ( FALSE == is_file_exist ( path ) )
		return TRUE;

	fd = ::open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd < 0) {
		log_printf(LOG_ERROR, "Failed to open unexport for writing!\r\n");
		return FALSE;
	}

	len = snprintf(buffer, BUFFER_MAX, "%d", gpio_pin);
	if (::write(fd, buffer, len) < 0) {
		log_printf(LOG_ERROR, "Fail to unexport gpio%d!\r\n", gpio_pin);
		::close(fd);
		return FALSE;
	}
	 
	::close(fd);
	return TRUE;
}


bool Gpio::set_direction( gpio_direction_t dir )
{
	char path[DIRECTION_MAX];
	int fd;

	if ( gpio_pin == GPIO_PIN_NONE ){
		return FALSE;
	}

	snprintf ( path , DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", gpio_pin );
	fd = ::open ( path , O_WRONLY);
	if (fd < 0) {
		log_printf(LOG_ERROR, "Failed to open gpio%d direction for writing!\r\n", gpio_pin );
		return FALSE;
	}

	if ( ::write ( fd , dir==GPIO_INPUT?"in":"out" , dir==GPIO_INPUT?2:3 ) < 0 ) {
		log_printf(LOG_ERROR, "Failed to set gpio%d direction!\r\n", gpio_pin);
		::close(fd);
		return FALSE;
	}

	::close(fd);
	return TRUE;
}


bool Gpio::write(gpio_value_t value )
{
	char path[DIRECTION_MAX];
	int fd;

	if ( gpio_pin == GPIO_PIN_NONE ){
		return FALSE;
	}

	snprintf( path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/value", gpio_pin);
	fd = ::open( path , O_WRONLY );
	if (fd < 0) {
		log_printf(LOG_ERROR, "Failed to open gpio%d value for writing!\r\n", gpio_pin);
		return FALSE;
	}

	if ( ::write( fd , value ? "1" : "0" , 1 ) < 0) {
		log_printf(LOG_ERROR, "Failed to write gpio%d value!\r\n", gpio_pin );
		::close(fd);
		return FALSE;
	}
	
	::close(fd);
	return TRUE;
}


bool Gpio::read(gpio_value_t &value )
{
	char path[DIRECTION_MAX];
	char value_str[3];
	int fd;
	
	if ( gpio_pin == GPIO_PIN_NONE ){
		return FALSE;
	}

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/value", gpio_pin);
	fd = ::open(path, O_RDONLY);
	if (fd < 0) {
		log_printf(LOG_ERROR, "Failed to open gpio%d value for reading!\r\n", gpio_pin);
		return FALSE;
	}

	if (::read(fd, value_str, 3) < 0) {
		log_printf(LOG_ERROR, "Failed to read gpio%d value!\r\n", gpio_pin);
		::close(fd);
		return FALSE;
	}

	::close(fd);
	value = (gpio_value_t)(atoi(value_str));
	return TRUE;
}


gpio_value_t Gpio::read(void)
{
	gpio_value_t value = GPIO_LOW;
	bool res = this->read( value );
	if ( FALSE == res ){
		log_printf(LOG_ERROR, "gpio %d read error!", gpio_pin );
	}
	return value;
}

