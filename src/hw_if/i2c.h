
#pragma once

#include <stdint.h>

typedef enum {
	I2C1 = 1,
	I2C2 = 2,
} i2c_t;

class I2c{

private:

	i2c_t i2c_hw;
	int fd;

	bool open();
	bool close();

public:

	I2c( i2c_t dev ){
		i2c_hw = dev;
		open();
	}
	~I2c(){
		close();
	}

	bool write(uint8_t slave_addr, uint8_t *buf, int len );
	bool read(uint8_t slave_addr, uint8_t *buf, int len );
};

