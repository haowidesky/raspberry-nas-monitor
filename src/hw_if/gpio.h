
#pragma once

#include "types.h"


// Defined according to the raspberry pie GPIO
typedef enum {
	GPIO_PIN_NONE = 0xFF,
	GPIO_PIN_2 = 2,
	GPIO_PIN_3,
	GPIO_PIN_4,
	GPIO_PIN_5,
	GPIO_PIN_6,
	GPIO_PIN_7,
	GPIO_PIN_8,
	GPIO_PIN_9,
	GPIO_PIN_10,
	GPIO_PIN_11,
	GPIO_PIN_12,
	GPIO_PIN_13,
	GPIO_PIN_14,
	GPIO_PIN_15,
	GPIO_PIN_16,
	GPIO_PIN_17,
	GPIO_PIN_18,
	GPIO_PIN_19,
	GPIO_PIN_20,
	GPIO_PIN_21,
	GPIO_PIN_22,
	GPIO_PIN_23,
	GPIO_PIN_24,
	GPIO_PIN_25,
	GPIO_PIN_26,
	GPIO_PIN_27,
} gpio_pin_t;

typedef enum{
	GPIO_INPUT,
	GPIO_OUTPUT,
} gpio_direction_t;

typedef enum{
	GPIO_LOW,
	GPIO_HIGH,
} gpio_value_t;

class Gpio{
private:
	int gpio_pin;

	bool open();
	bool close();

#define POUT				4 /* P1-07 */
#define BUFFER_MAX			3
#define DIRECTION_MAX 		48
	
public:
	Gpio( gpio_pin_t pin ){
		gpio_pin = pin;
		this->open();
	}
	~Gpio(){
		this->close();
	}

	bool set_direction( gpio_direction_t dir );
	bool write( gpio_value_t value );
	bool read( gpio_value_t &value );
	gpio_value_t read(void);
	
};

