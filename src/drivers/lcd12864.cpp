
#include "lcd12864.h"
#include <unistd.h>
#include <string.h>


Lcd12864::Lcd12864(Gpio *gpio_cs, Gpio *gpio_dat, 
	Gpio *gpio_clk, Gpio *gpio_bl)
{
	cs = gpio_cs;
	dat = gpio_dat;
	clk = gpio_clk;
	bl = gpio_bl;

	cs->set_direction(GPIO_OUTPUT);
	dat->set_direction(GPIO_OUTPUT);
	clk->set_direction(GPIO_OUTPUT);
	clk->set_direction(GPIO_OUTPUT);

	cs->write(GPIO_LOW);
	dat->write(GPIO_HIGH);
	clk->write(GPIO_HIGH);
	bl->write(GPIO_LOW);

	init();
}


Lcd12864::~Lcd12864()
{

}


void Lcd12864::delay(uint32_t ms)
{
	usleep(ms);
}


void Lcd12864::write_data(uint8_t type, uint8_t data)
{
	uint8_t H_data,L_data,i;
	cs->write(GPIO_HIGH);

	for(i=0;i<8;i++)					//这里写一个起始字节
	{
		if( type & 0x80 ){
			dat->write(GPIO_HIGH);
		}else{
			dat->write(GPIO_LOW);
		}
		type <<= 1;
		delay(1);
		clk->write(GPIO_LOW);
		delay(1);
		clk->write(GPIO_HIGH);			//由低到高的时候，提取数据
	}

	H_data = data & 0xf0;				//因为ST7920的特殊性，一个字节要改成两个字节传送，所以这里将第四位变成0
	L_data = data & 0x0f;
	L_data <<= 4;
	for(i=0;i<8;i++)						//这里写数据
	{
		if(H_data&0x80){
			dat->write(GPIO_HIGH);
		}else{
			dat->write(GPIO_LOW);
		}
		delay(1);
		H_data	<<=	1;
		clk->write(GPIO_LOW);
		delay(1);
		clk->write(GPIO_HIGH);
		delay(1);
	}

	for(i=0;i<8;i++)						//这里写数据
	{
		if(L_data&0x80){
			dat->write(GPIO_HIGH);
		}else{
			dat->write(GPIO_LOW);
		}
		L_data <<= 1;

		clk->write(GPIO_LOW);
		delay(1);
		clk->write(GPIO_HIGH);
		delay(1);
	}
	delay(1);
	cs->write(GPIO_LOW);
}



void Lcd12864::init()
{
	write_data(WRITE_COMMAND,CLOSE_EXTEND);			//关闭扩充指令集
	delay(1);
	write_data(WRITE_COMMAND,DISPLAY_ON);			//开显示
	delay(1);
	write_data(WRITE_COMMAND,CLEAN_SCREEN);			//清屏
	delay(1);
	write_data(WRITE_COMMAND,0x06);					//光标回0
	delay(1);

	clear_display();
}


void Lcd12864::backlight_set(bool on)
{
	if (on){
		bl->write(GPIO_HIGH);
	}else{
		bl->write(GPIO_LOW);
	}
}

void Lcd12864::clear_display()
{
	write_data(WRITE_COMMAND, CLEAN_SCREEN);
}


void Lcd12864::write_string(uint8_t ypos, uint8_t xpos , const char *str )
{
	int i;
	int length = strlen((char*)str);
	if ( length + xpos > 16 )
		length = 16 - xpos;

	switch( ypos ){
		case 0:
			ypos = 0x80;
			break;
		case 1:
			ypos = 0X90;
			break;
		case 2:
			ypos = 0X88;
			break;
		case 3:
			ypos = 0X98;
			break;
		default:
			return;
	}
	write_data(WRITE_COMMAND, ypos+xpos);

	for ( i=0;i<length;i++ ){
		write_data(WRITE_DATA, str[i]);
	}
}




void Lcd12864::draw_picture(const uint8_t pic[64][128])
{
	write_data(WRITE_COMMAND, OPEN_EXTEND);			// 打开扩充指令集
	write_data(WRITE_COMMAND, OPEN_DRAWING);		// 开启绘图功能

	uint8_t i,j;
	uint8_t *p = (uint8_t *)pic;

	for(i=0;i<32;i++) {      						//上半屏的32排依次先写满
	write_data ( WRITE_COMMAND , 0x80+i ); 			//先送垂直地址
        write_data ( WRITE_COMMAND , 0x80 );   		//再送水平地址，水平地址可自加1
        for(j=0;j<16;j++){							//每排128个点，所以一共要16个两位16进制数（也就是8位二进制数）才能全部控制
            write_data( WRITE_DATA , *p++ );
        }
    }

    for(i=0;i<32;i++) {    							//下半屏的32排操作原理和上半屏一样
        write_data ( WRITE_COMMAND , 0x80+i);
        write_data ( WRITE_COMMAND , 0x88);
        for(j=0;j<16;j++) {
            write_data( WRITE_DATA , *p++ );
        }
    }
	write_data ( WRITE_COMMAND , CLOSE_EXTEND );
//	lcd12864_write ( WRITE_COMMAND , CLOSE_DRAWING );			// 关闭绘图功能
}




