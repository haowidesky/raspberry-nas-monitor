

#ifndef _LCD12864_H_
#define _LCD12864_H_

#include "gpio.h"


class Lcd12864{
private:

#define WRITE_COMMAND		0xf8
#define WRITE_DATA			0xfa

/********在基本指令集下的指令********************/
#define	CLEAN_SCREEN		0x01						//清屏函数	0000 0001
#define	HOME				0x02						//位址归位 0000 0010
#define DISPLAY_ON			0x0c						//开游标，开显示 	0000 1110
#define CURSOR				0x1a
	
/***********基本，扩充指令集切换函数***************/
#define OPEN_EXTEND			0x34						//开启扩充指令集	，关闭绘图功能
#define	CLOSE_EXTEND		0x30						//开启基本指令集
#define OPEN_DRAWING		0x36						//开启绘图功能
#define CLOSE_DRAWING		0x34						//关闭绘图功能
		
	Gpio *cs;
	Gpio *dat;
	Gpio *clk;
	Gpio *bl;

	void delay(uint32_t ms);
	void write_data(uint8_t cmd, uint8_t data);
	void init();

	
public:

	Lcd12864(Gpio *gpio_cs, Gpio *gpio_dat, Gpio *gpio_clk, Gpio *gpio_bl);
	~Lcd12864();

	void backlight_set(bool on);
	void clear_display();
	void write_string(u8 ypos, u8 xpos, const char * str);
	void draw_picture(const uint8_t pic[64][128]);

};


#endif

