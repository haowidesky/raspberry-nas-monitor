
#pragma once

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/time.h>  
#include <time.h>


#define CONSOLE_COLOR_BLACK				"\033[30m"
#define CONSOLE_COLOR_RED				"\033[31m"
#define CONSOLE_COLOR_GREEN				"\033[32m"
#define CONSOLE_COLOR_YELLOW			"\033[33m"
#define CONSOLE_COLOR_BLUE				"\033[34m"
#define CONSOLE_COLOR_PURPLE			"\033[35m"		//��ɫ
#define CONSOLE_COLOR_BLUESKY			"\033[36m"		//����
#define CONSOLE_COLOR_WHITE				"\033[37m"
#define CONSOLE_COLOR_END				"\033[0m"
typedef const char* CONSOLE_COLOR;

typedef enum {
	LOG_ERROR,
	LOG_WARNING,
	LOG_INFO,
	LOG_DEBUG,
} LOG_LEVEL;

#define LOG_COLOR_ERROR			CONSOLE_COLOR_RED
#define LOG_COLOR_WARNING		CONSOLE_COLOR_YELLOW
#define LOG_COLOR_INFO			CONSOLE_COLOR_BLUESKY
#define LOG_COLOR_DEBUG			CONSOLE_COLOR_WHITE
#define LOG_COLOR_TIME			CONSOLE_COLOR_GREEN
#define LOG_COLOR_FILE_LINE		CONSOLE_COLOR_WHITE

class Log{
private:
	LOG_LEVELprint_level;

	void print_time( CONSOLE_COLOR color ){
		struct timeval tv;  
	    struct tm *time_ptr;  
	    memset(&tv, 0, sizeof(timeval));  
	    gettimeofday(&tv, NULL);  
	    time_ptr = localtime(&tv.tv_sec);
	    color_print(color, "[%4d-%02d-%02d %02d:%02d:%02d.%06d]",
	            time_ptr->tm_year + 1900,
	            time_ptr->tm_mon + 1,
	            time_ptr->tm_mday,
	            time_ptr->tm_hour,
	            time_ptr->tm_min,
	            time_ptr->tm_sec,
	            (int)tv.tv_usec
	           );
	}

	void color_print( CONSOLE_COLOR color, const char *fmt, ... ){
		printf( "%s", color );
		va_list args;
	    va_start(args,fmt);
	    vprintf(fmt,args);
	    va_end(args);
		printf( CONSOLE_COLOR_END );
	}

public:
	static Log *build(){
		Log *log = new Log();
		return log;
	}

	Log():print_level(LOG_DEBUG){
	}
	~Log(){}

	void print( 
							LOG_LEVEL  level, 
							const char *file, 
							int line, 
							const char *fmt, ... 
						){
						
		if ( level > print_level ){
			return;
		}
		print_time( LOG_COLOR_TIME );
		CONSOLE_COLOR color;
		if ( level == LOG_ERROR ){
			color = LOG_COLOR_ERROR;
		}else if ( level == LOG_WARNING ){
			color = LOG_COLOR_WARNING;
		}else if ( level == LOG_INFO ){
			color = LOG_COLOR_INFO;
		}else{
			color = LOG_COLOR_DEBUG;
		}

		printf ( "%s", color );
		va_list args;
	    va_start(args,fmt);
	    vprintf(fmt,args);
	    va_end(args);

		printf( CONSOLE_COLOR_END );

		if ( level <= LOG_WARNING ){
			color_print ( LOG_COLOR_FILE_LINE , "\tIn file:%s line:%d\r\n", file, line);
		}
	}
	
	void set_level( LOG_LEVEL level ){
		print_level = level;
	}

};


extern Log *log;


#define log_printf(level,...)								\
	do{														\
		log->print(level,__FILE__,__LINE__,__VA_ARGS__ );	\
	}while(0)

