
#pragma once

#include "types.h"
#include "log.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <stdlib.h>

#define IPCKEY 				0x111

#define MSG_KEY_DEFAULT		1024
#define MSG_TEXT_SIZE		10240


typedef enum {
	MSG_TYPE_SYSTEM_STATE_CHANGED = 1,
	MSG_SENSIRION_INFO_UPDATE,
	MSG_SYSTEM_PARAMETERS_UPDATE,
	MSG_TYPE_DEBUG,
	MSG_TYPE_EXIT,
} MSG_TYPE;


struct msgstru  
{  
	long msgtype;
	MSG_TYPE type;
	int length;
	char text[MSG_TEXT_SIZE];
};


class MsgQueue
{
private:
	int msq_key;
	int msqid;
	
public:

	struct msgstru msgs;

	static MsgQueue *build(){
		MsgQueue *mq = new MsgQueue();
		return mq;
	}
	
	MsgQueue(){
		msq_key = MSG_KEY_DEFAULT;
		msqid = msgget(msq_key,IPC_EXCL); /*检查消息队列是否存在*/
		if ( msqid >= 0 ){
			msgctl(msqid,IPC_RMID,0); //删除消息队列
		}
		
		msqid = msgget(msq_key,IPC_CREAT|0666);/*创建消息队列*/
		if(msqid <0){  
			log_printf ( LOG_ERROR, "Failed to create msq | errno=%d [%s]\n",errno,strerror(errno) );
		}else{
			log_printf(LOG_INFO, "Create Msg Queue Successful.\n");
		}		
	}
	~MsgQueue(){
		if ( msqid >= 0 )
			msgctl(msqid,IPC_RMID,0); //删除消息队列
	}
	
	/* 发送消息队列 */
	BOOL send(struct msgstru msgs, int msgflg = IPC_NOWAIT ){
		int ret_value = msgsnd(msqid,&msgs,sizeof(struct msgstru)-MSG_TEXT_SIZE+msgs.length, msgflg);  
		if ( ret_value < 0 ) {
			log_printf(LOG_ERROR, "msgsnd() write msg failed,errno=%d[%s]\n",errno,strerror(errno));
			return FALSE;
		}
		return TRUE;
	}
	BOOL send(MSG_TYPE type, char *msgtext=NULL, int len=0, int msgflg = IPC_NOWAIT ){
		struct msgstru msgs;
		msgs.msgtype = 1;
		msgs.type = type;
		msgs.length = len;
		if ( NULL != msgtext && len > 0 )
			memcpy( msgs.text, msgtext, len );
		return send( msgs , msgflg );
	}

	BOOL recv( struct msgstru *msgs ){
		ssize_t ret_value = msgrcv(msqid, msgs, sizeof(struct msgstru), 0, 0);
		if ( ret_value < 0 ){
			return FALSE;
		}
		return TRUE;
	}

	BOOL recv( void ){
		return recv( &msgs );
	}
	
};

extern MsgQueue *msg_queue;

