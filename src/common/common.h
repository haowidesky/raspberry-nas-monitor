
#pragma once

#include <stdint.h>
#include <sys/types.h>


#define ARRAY_SIZE(x)			(sizeof(x)/sizeof(*(x)))
#define foreach(i,array)		for(size_t i=0;i<ARRAY_SIZE(array);i++)
#define for_range(i,start,end)	for(int i=start;i<end;i++)

