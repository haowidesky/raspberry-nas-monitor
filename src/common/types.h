﻿
#ifndef _TYPES_H_
#define _TYPES_H_

#include <stdint.h>

typedef unsigned char 		u8;
typedef unsigned short 		u16;
typedef unsigned int 		u32;
typedef unsigned long long	u64;
typedef signed char 		s8;
typedef signed short		s16;
typedef signed int 			s32;
typedef signed long long	s64;

typedef unsigned int 		uint;
typedef unsigned long 		ulong;

/*
typedef enum {
	false = 0,
	true,
} bool;
*/

typedef enum{
	FALSE = 0,
	TRUE
} BOOL;

typedef enum {
	RES_OK = 0,
	RES_ERR,
} RESULT;

typedef enum {
	NO = 0,
	YES,
} YES_OR_NO;

#ifdef __cplusplus
#define null 	0
#else
#define null 	(void *)0
#endif

//#define NULL 	(void *)0

#endif

