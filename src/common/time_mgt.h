
#pragma once

#include "types.h"
#include "common.h"

#include <unistd.h>
#include <time.h>  


class TimeMgt{

private:
	ulong timestamp;
public:

	TimeMgt(){
		timestamp = get_ms_count();
	}
	~TimeMgt(){
		
	}

	// 返回自系统开机以来的毫秒数（tick）  
	static ulong get_ms_count(){
	    struct timespec ts;
	    clock_gettime(CLOCK_MONOTONIC, &ts);
	    return (ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
	}
	static void sleep_ms( uint time ){
		usleep( time * 1000 );
	}

	void reset(){
		timestamp = get_ms_count();
	}

	BOOL is_time_up( ulong time ){
		ulong now = get_ms_count();
		if ( now - timestamp >= time ){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	ulong get_elapsed_time(){
		ulong now = get_ms_count();
		return now - timestamp;
	}

};

