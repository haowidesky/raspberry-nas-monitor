

#include "main.h"
#include "log.h"
#include "gpio.h"
#include "lcd12864.h"

#include <unistd.h>


int main( int argc, char **argv )
{
	log = Log::build();
	log->set_level(LOG_DEBUG);
	log_printf(LOG_INFO, "Startup APP ...\r\n");

	Gpio *lcd12842_cs_pin = new Gpio(GPIO_PIN_13);
	Gpio *lcd12842_dat_pin = new Gpio(GPIO_PIN_19);
	Gpio *lcd12842_clk_pin = new Gpio(GPIO_PIN_26);
	Gpio *lcd12842_bl_pin = new Gpio(GPIO_PIN_6);
	Lcd12864 *lcd = new Lcd12864(lcd12842_cs_pin, lcd12842_dat_pin,
		lcd12842_clk_pin, lcd12842_bl_pin);

	lcd->write_string(0, 0, "����");
	lcd->backlight_set(true);

	while(true){
		sleep(100);
	}
	
	return 0;
}


