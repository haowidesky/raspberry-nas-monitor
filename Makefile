﻿
# 可执行文件  
TARGET=app

# 依赖目标 SRCS    INCS

#SRCS = \
#src/main/main.cpp \
#src/decon_system/decon_system.cpp \
#src/decon_system/host.cpp \
#src/decon_system/vent_line_subsystem.cpp 

SRCS = $(wildcard \
src/main/*.cpp \
src/common/*.cpp \
src/drivers/*.cpp \
src/hw_if/*.cpp \
) 

INCS = \
-I"src/common" \
-I"src/drivers" \
-I"src/main" \
-I"src/hw_if"


COLOR_BLACK =   	"\033[30m"
COLOR_RED =     	"\033[31m"
COLOR_GREEN =   	"\033[32m"
COLOR_YELLOW =  	"\033[33m"
COLOR_BLUE =    	"\033[34m"
COLOR_PURPLE =  	"\033[35m"
COLOR_SKYBLUE = 	"\033[36m"
COLOR_WHITE =   	"\033[37m"
COLOR_END = 		"\033[0m"


# 目标文件
OBJS = $(SRCS:.cpp=.o)
#OBJS = $(patsubst <pattern>,<replacement>,<text> ) 
#OBJS = $(patsubst %.cpp, $(BUILD_DIR)/%.o,$(SRCS) )


# 指令编译器和选项
CC=g++
CPPFLAGS=-Wall -g $(INCS)

# 链接时的选项
LINKFLAGS=-lpthread 

all: clean $(TARGET)

# 生成可执行文件
$(TARGET):$(OBJS)
	@echo "  "$(COLOR_GREEN)Create execute file:$(COLOR_END) $(COLOR_PURPLE)$@$(COLOR_END)
	@$(CC) -o $@ $^ $(LINKFLAGS)
 
clean:
	@echo $(COLOR_SKYBLUE)"    \c"
	@rm -rf $(TARGET) $(OBJS)
	@echo "Cleanup over!"$(COLOR_END)
  
# 连续动作，先清除再编译链接，最后执行  
exec:clean $(TARGET)
	@echo 开始执行
	sudo ./$(TARGET)
	@echo 执行结束
	

# 编译规则 $@代表目标文件 $< 代表第一个依赖文件  
%.o:%.cpp
	@echo $(COLOR_YELLOW)"    "Compiling$(COLOR_END) $(COLOR_RED)$<$(COLOR_END)
	@$(CC) $(CPPFLAGS) -o $@ -c $< 


